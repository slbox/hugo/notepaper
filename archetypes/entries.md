---
adv: true		# Disable display advertisement on this entry
date: {{- time.Now.Format .Site.Data.General.Time.Format.full -}}
thumbnail: ""	# Remove to use default thumbnail
topics: ""		# One and only one
series: "" 		# A taxonomy to group the series entries.
tags: []		# Shared ideas with other entries
title: {{- (replace .File.TranslationBaseName "-" " ") | title -}}
summary: "" 	# Try your best to make a 160 characters summary
draft: true
---
