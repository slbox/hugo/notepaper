Thumbnail credit https://unsplash.com/photos/N135eczYTAs


# Notepaper

>   Just a paper was typed from a [typewriter][lnk0]  
> — me said.

## Highlight content
In case you busy:
- [](#)
- [](#)
- [](#)
- [](#)

## License
[MIT License](/LICENSE)  
There, you're free to go. But if you are nice, could you credit me in your site/work if you use my theme?

https://www.coderstool.com/share-social-link-generator

# 


<!-- 
The Notepaper theme was built to server one purpose, fun - I mean blogging. I made this theme with a mind is all into vintage minimalism.

## Highlight content
In case you busy:
- [How to install](#How-to-install)
- [Theme features](#Features)
- [Dev tools](#Dev-tools)
- [Troubleshooting](#Troubleshooting)



## MIT license
See license file  
This is official root repository of Notepaper theme.

## Features
- Monospace font only
	- Support English language
	- Support Vietnamese language
- Reponsive design
- Dark theme

## How to install
If no modification is needed, all the content in [/build](/build) directory to the `theme` folder


## Files
- `.gitignore` : ignore trash files (so it won't be pushed to repo).
- `config.toml` : recommend setting for the theme.
- `*/readme.md` : a note for the dir of its.


## Dev tools
- [Hugo](https://gohugo.io): static site generator using Markdown format as database.
- [NodeJs]()


# Troubleshooting
This is just a small personal (fun) project that means bugs should be common friends. To report bugs and get help, please open issue in this Gitlab repo. Leave your issue description (and if any, a screenshot would be great too).

## F.A.Q.
Nothing to do yet, so... how about listen [a favourite song of mine](https://youtu.be/ayE6IKs25J8)?-->

[lnk0]: https://duckduckgo.com/?q=typing+writer&t=ffab&ia=about