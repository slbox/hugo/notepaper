---
notice: true
---

This section is called homepage bulletin. It is used to display notice of author to guest. Very useful to do something like this: "Yo! I'm off for vacation so there won't be any new posts for the next decade. Don't be sad ok, I'll be back in a flash!" Or just announce that you are moving host so the site will be down for mili-century. Suite yourself.

By the way, this notice can be turned off until cookie is cleared.