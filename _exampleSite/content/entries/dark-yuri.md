---
title: "Dark Yuri"
date: 2019-09-14T08:22:57+07:00
draft: false
series: Yuri World
---

## Move static content to `static`
Jekyll has a rule that any directory not starting with `_` will be copied as-is to the `_site` output. Hugo keeps all static content under `static`. You should therefore move it all there.
With Jekyll, something that looked like