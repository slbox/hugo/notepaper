---
date: 2019-03-10
category: ads
excerpt: The Notepaper theme was built to server one purpose, fun - I mean my blog. I made this theme with a mind is all into minimalism and old taste.
recomend: /tutorials/mathjax
title: Hello World
weight: 10
---

# Notepaper
>   Just a paper was typed from a [typewriter][lnk0]  
> — Đăng Tú said

The Notepaper theme was built to server one purpose, fun - I mean my blog. I made this theme with a mind is all into minimalism and old taste.

{{< vidur WBtpWcG true >}}
	{{< credit "AggressivelyMeows" "https://imgur.com/gallery/WBtpWcG" >}}
{{< /vidur >}}

## Highlight content
In case you busy:

- [How to install](#How-to-install)
- [Theme features](#Features)
- [Dev tools](#Tools)
- [Troubleshooting](#Troubleshooting)



## MIT license
See license file  
This is official root repository of Notepaper theme.

## Features
- Monospace font only
	- Support English language
	- Support Vietnamese language
- Reponsive design
- Switching theme from client side.
	- Dark theme

## How to install


# Notes
Here are some notes to walk us through Notepaper's repo. It also has some recommended tools for build theme so be sure to check out [the Tools section](#Tools).

For further customization, see repo wiki.

## Repo. Structure
The repo structure directories has 2 parts. The first part start with a dot `.` in dir name. These dirs usually store data, setting and released theme files. The second part is testing ground.

## First part dir
- `.mockup` : mockup HTML code. Detail is in [`.mockup/readme.md`](/.mockup/readme.md).
- `.rv` stands for Released Version.
- `.vscode` : workspace setting of Visual Studio Code.

## Second part dir
All of dirs below are used for testing.
- `content` : dummy post for theme testing.
- `static` : dummy static data for theme testing.
- `themes` : HTML + Go Template format used by Hugo generator.

**Warning**: The theme in `themes/` is just a beta version and not ready to use!


## Files
- `.gitignore` : ignore trash files (so it won't be pushed to repo).
- `config.toml` : recommend setting for the theme.
- `*/readme.md` : a note for the dir of its.


## Tools
### Requirement:
- [Hugo](https://gohugo.io): static site generator using Markdown format as database.
- [Sass](http://sass-lang.com): CSS framework that helps developers keep their big-fat CSS to be clean.

### Recommendation:
- [JSCompress](https://jscompress.com): a webapp minifier for Js.


# Troubleshooting
This is just a small personal (fun) project that means bugs should be our common friends. To report bugs and get help, please open issue in this Gitlab repo. Leave your issue description (and if any, screenshot would be great too).

## F.A.Q.
Nothing to do yet, so... how about listen [a favourite song of mine](https://youtu.be/ayE6IKs25J8)?


[lnk0]: https://duckduckgo.com/?q=typing+writer&t=ffab&ia=about