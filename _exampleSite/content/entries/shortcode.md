# Syntax

## The simple ones
Some shortcodes support 2 style, some only one. See the comment at top of each shortcodes to [learn more](#shortcode-doc).

**Ordered style**: with arguments in order
```Markdown
{{</* shortcode "str1" 2 /*/>}}
```

**Label style**: with arguments label
```Markdown
{{</* shortcode one="str1" two=2 /*/>}}
```

**Warning**: shortcodes' arguments can only used either of style syntaxs.


## The nest one
This works like HTML tag, so its design is pretty friendly and easy to use:

```Markdown
{{</* shortcode one="str1" two=2 */>}}
	Whatever we wanna add in.
{{</* /shortcode */>}}
```


# Shortcode doc
It's usually a single line on top of each shortcodes. Something like this:
```Go
// No1
{{/*  {{</* vidur 0:"name of video" 1:autoplay 2:control 3:notLoop */>}}  */}}


// No2
{{/*  {{</* credit 0/name:"person or org name" 1/url:"credit-url" /*/>}}  */}}
```

- The 1st word is shortcode name.
- The rest, which seperated by a space, are arguments. Each arguments has 2 parts which are divided by a colon `:`.
	- The 1st part may have:
		- Start by a number means it could be used as ordered syntax style.
		- Start by a word means it could be used as label syntax style.
		- Start by a number/word means it could be used as both styles.
	- The 2nd part may have:
		- String: wrapped in a pair of double quotes `"`	
		- Number: pure number
		- Bool: similar to string but don't have double quotes.
- The very last one at the end is forward-slash `/`. If it means that this shortcode support nest feature.
	- NOTICE: If not use it, the slash must a the end of the code.
