const isDev = true;

const { src, dest, series } = require('gulp')
const path = require('path')
const glob = require('glob');
const sass = require('gulp-dart-sass')
const babel = require('gulp-babel')


const raw = {
	scss: './sass/**/*.scss',
	core: './babel-core/**/*.js',
	main: './babel-main/**/*.js'
}

const map = {
	src: {
		sourcemaps: isDev
	},
	dest: isDev ?
		{ sourcemaps: '.' } :
		null
}

const theme = isDev ? path.join('_exampleSite', 'themes', 'Notepaper') : ''
const static = dest(
	path.join('..', theme, 'static'),
	map.dest
)


const toCSS = () => {
	const opt = {
		outputStyle: 'compressed'
	}

	return src(raw.scss, map.src)
		.pipe(sass(opt))
		.pipe(static)
}

const minJs = srcRaw => {
	return src(srcRaw, map.src)
		.pipe(babel())
		.pipe(static)
}
const minCore = () => {
	return minJs(raw.core)
}
const minMain = () => {
	return minJs(raw.main)
}

/**
 * Copy theme files to `/_exampleSite` for testCase
 */
const cpTheme = () => {
	const globs = glob.sync(
		'../**/*',
		{
			ignore: [
				'../_*/**/*',
				'../.*/**/*',
				'../LICENSE',
				'../**/readme.md',
				'../README.md'
			]
			// Doesn't completely remove `_` at all, not cool!
		}
	)
	const opt = {
		base: '..'
	}
	const end = dest(path.join('..', theme))

	return src(globs, opt)
		.pipe(end)
}

exports.cpThm = cpTheme
exports.default = series(toCSS, minCore, minMain)